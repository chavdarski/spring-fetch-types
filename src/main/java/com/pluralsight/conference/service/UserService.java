package com.pluralsight.conference.service;

import com.pluralsight.conference.model.Registration;
import com.pluralsight.conference.model.User;

import java.util.List;

public interface UserService {
    User save(User user);

    List<User> findAllSession();
}
