package com.pluralsight.conference;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:application.properties")
public class ConferenceConfig implements WebMvcConfigurer {

    private final String dbUrl, DbUsername, DbPassword, dbDriver;
    private final Properties dbProperties;

    public ConferenceConfig(final Environment env) {
        this.dbUrl = env.getProperty("spring.datasource.url");
        this.DbUsername = env.getProperty("spring.datasource.username");
        this.DbPassword = env.getProperty("spring.datasource.password");
        this.dbDriver = env.getProperty("spring.datasource.driver-class-name");
        this.dbProperties = new Properties();
        dbProperties.setProperty("hibernate dialect", "spring.jpa.properties.dialect");
    }

    @Bean(name="entityManagerFactory")
    public LocalSessionFactoryBean sessionFactory(){
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(datasource());
        sessionFactory.setPackagesToScan("com.pluralsight.conference.model");
        sessionFactory.setHibernateProperties(dbProperties);
        return sessionFactory;
    }

    @Bean
    public DataSource datasource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dbDriver);
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(DbUsername);
        dataSource.setPassword(DbPassword);
        return dataSource;
    }
}
