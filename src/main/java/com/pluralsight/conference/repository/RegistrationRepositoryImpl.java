package com.pluralsight.conference.repository;

import com.pluralsight.conference.model.Registration;
import com.pluralsight.conference.model.RegistrationReport;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class RegistrationRepositoryImpl implements RegistrationRepository {

    private final SessionFactory sessionFactory;
    private final EntityManager entityManager;

    @Autowired
    public RegistrationRepositoryImpl(SessionFactory sessionFactory, EntityManager entityManager) {
        this.sessionFactory = sessionFactory;
        this.entityManager = entityManager;
    }

    @Override
    public Registration save(Registration registration) {
        entityManager.persist(registration);
        return registration;
    }

    @Override
    public List<Registration> findAll() {
            List<Registration> result = entityManager
                    .createQuery("Select r from Registration r where id < 5", Registration.class)
                    .getResultList();
            entityManager.close();
            return result;

    }

    @Override
    public List<Registration> findAllSession() {
        try (Session session = sessionFactory.openSession()) {
            Query<Registration> query = session.createQuery("from Registration", Registration.class);
            return query.list();
        }
    }

    @Override
    public List<RegistrationReport> findAllReports() {
        try (Session session = sessionFactory.openSession()) {
            Query<RegistrationReport> query = session.createNamedQuery(Registration.REGISTRATION_REPORT, RegistrationReport.class);
            return query.list();
        }
    }

}
