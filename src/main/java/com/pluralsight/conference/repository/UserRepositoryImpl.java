package com.pluralsight.conference.repository;

import com.pluralsight.conference.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @PersistenceContext
    private EntityManager entityManager;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User save(User user) {

        entityManager.persist(user);

        return user;
    }

    @Override
    public List<User> findAllSession() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("from User", User.class).list();
        }
    }

}
