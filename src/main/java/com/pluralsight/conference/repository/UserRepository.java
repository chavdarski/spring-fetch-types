package com.pluralsight.conference.repository;

import com.pluralsight.conference.model.User;

import java.util.List;

public interface UserRepository {
    User save(User user);

    List<User> findAllSession();
}
